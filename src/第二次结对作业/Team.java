package 第二次结对作业;

import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.io.File;
import java.io.FileInputStream;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public class Team {
	public static String URL;
	public static String COOKIE;
	public static void main(String[] args) throws IOException {
		getResources();   //读取源文件的url以及cookie
		ArrayList<String> baseList = baseList();    //获取所有课堂完成部分的url
		List<Student> students = stuList(baseList);	//学生信息
		Collections.sort(students,new Student());	//排序
		write(students);
	}

	public static void getResources() {
		Properties prop = new Properties();  //从配置文件中加载配置项：使用load方法
		try {
			prop.load(new FileInputStream("resources/config.properties"));
		} catch (Exception a) {
			a.printStackTrace();
		}
		URL = prop.getProperty("url");		//读取配置项中的结果：使用getProperty方法
		COOKIE = prop.getProperty("cookie");
	}
	


	public static ArrayList<String> baseList() throws IOException {
	    Document document = Jsoup.connect(URL).header("cookie", COOKIE).get();   // 手动设置cookies,解析html
	    if (document != null) {
	        //获取到所有活动的div
	        Elements activDivs = document.getElementsByClass("interaction-row"); 
			ArrayList<String> baseList = new ArrayList<String>();  //初始化存储课堂完成部分链接的列表
	        //获取课堂完成部分活动的url
	        for(int i = 0;i < activDivs.size();i++) {
	        	if(activDivs.get(i).child(1).child(0).child(1).text().indexOf("课堂完成") != -1) {
	        		//将data-url的值转为字符串
	        		String urlString = activDivs.get(i).attr("data-url").toString();
	        		//把值加到baseList
		        	baseList.add(urlString);
		        }
	        }
	        return baseList;
	    } else {
	        System.out.println("出错啦！！！！！");
	        return null;
	    }
	}

	public static ArrayList<Student> stuList(ArrayList<String> baseList) throws IOException{
		//初始化列表存储html文件的Document对象
		ArrayList<Document> baseActives = new ArrayList<Document>(baseList.size()); 
		ArrayList<Student> stuList = new ArrayList<>();		//学生列表(包含每个活动的每个学生）
		for(int i=0;i<baseList.size();i++) {
			//解析课堂完成部分
			Document document = Jsoup.connect(baseList.get(i)).header("cookie", COOKIE).get();
			baseActives.add(document);	//将解析后的document添加到baseActives
		}
		for(int j=0;j<baseActives.size();j++) {
			//获取每一个课堂完成部分网页中的class="homework-item"的div
			Elements baseStuDivs = baseActives.get(j).getElementsByClass("homework-item");
			for(int k=0;k<baseStuDivs.size();k++) {
				try {
					Student stu = new Student();
					stu.setStuNo(baseStuDivs.get(k).child(0).child(1).child(1).text().toString());  //设置学号
					stu.setStuName(baseStuDivs.get(k).child(0).child(1).child(0).text().toString());  //设置姓名
					String score = baseStuDivs.get(k).child(3).child(1).child(1).text();  //文本
					//需要将没被评分的以及没有提交的成绩设置为0
					if(baseStuDivs.get(k).child(3).child(0).child(1).text().contains("尚无评分")) {
						stu.setStuScore(0.0);
					}
					else if (baseStuDivs.get(k).child(1).child(0).text().contains("未提交")) {
						stu.setStuScore(0.0);
					}
					else {
						stu.setStuScore(Double.parseDouble(score.substring(0, score.length() - 2)));  //设置成绩
					}
					stuList.add(stu);
				} catch (Exception e) {
				}
			}
		}

		ArrayList<Student> studentList = new ArrayList<>();	//去掉重复后的学生列表
		Double scoreDouble;	//每个学生的总成绩
		//初始化每个学生的总成绩
		for(int i=0;i<stuList.size();i++) {
			scoreDouble = 0.0;
			Student student = new Student();
			for(int j=i+1;j<stuList.size();j++) {
				if(stuList.get(i).getStuName().contains(stuList.get(j).getStuName())) {       //计算成绩
					scoreDouble += stuList.get(j).getStuScore();
					stuList.remove(j);	//删重
				}
			}
			student.setStuNo(stuList.get(i).getStuNo());
			student.setStuName(stuList.get(i).getStuName());
			student.setStuScore(scoreDouble);
			studentList.add(student);
		}
//		System.out.println(studentList.size());
//		for (int k = 0; k < studentList.size(); k++) {
//			System.out.println(studentList.get(k).toString());
//		}
		return studentList;
	} 
	public static void write(List<Student> students) throws FileNotFoundException {
		File file = new File("score.txt");      //指定在哪输出
		PrintWriter printWriter = new PrintWriter(new FileOutputStream(file),true);
		double ave = 0.0;       //平均分初始为0
		for (int j = 0; j < students.size(); j++) {
			ave += students.get(j).getStuScore();
		}       //对所有学生的成绩进行累加
		ave = ave/students.size();      //总成绩除以我们学生的人数算出平均分
		printWriter.println("最高经验值"+students.get(0).getStuScore()+",最低经验值"+students.get(students.size()-1).getStuScore()+",平均经验值"+ave);
		for (int i = 0; i < students.size(); i++) {
			printWriter.println(students.get(i).toString());
		}
        //输出
		printWriter.close();
	}
}

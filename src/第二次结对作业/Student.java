package 第二次结对作业;

import java.util.Comparator;

public class Student implements Comparator<Student>{
	public String stuNo;
	public String stuName;
	public Double stuScore;
	public String getStuNo() {
		return stuNo;
	}
	public void setStuNo(String stuNo) {
		this.stuNo = stuNo;
	}
	public String getStuName() {
		return stuName;
	}
	public void setStuName(String stuName) {
		this.stuName = stuName;
	}
	public Double getStuScore() {
		return stuScore;
	}
	public void setStuScore(Double stuScore) {
		this.stuScore = stuScore;
	}
	@Override
	public String toString() {
		return stuNo + "," + stuName + "," + stuScore;
	}
	@Override
	public int compare(Student o1, Student o2) {
		// TODO Auto-generated method stub
		int sn = Integer.parseInt(o1.getStuNo()) - Integer.parseInt(o2.getStuNo());
		int score = (int) (o2.getStuScore() - o1.getStuScore());
		return score==0?sn:score;
	}
}